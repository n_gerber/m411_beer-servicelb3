package ch.tbz.beerservice.styles;

public class Style {

	private int id;
	private String name;
	private String description;
	private int categoryId;

	public Style(int id, String name, String description, int categoryId) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.categoryId = categoryId;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public int getCategoryId() {
		return categoryId;
	}

	@Override
	public String toString() {
		return this.id + "::" + this.name;
	}
}
