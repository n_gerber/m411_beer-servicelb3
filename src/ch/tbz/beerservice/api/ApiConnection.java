package ch.tbz.beerservice.api;

import ch.tbz.beerservice.beers.Beer;
import ch.tbz.beerservice.styles.Style;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

public class ApiConnection {

  private static String baseUrl = "http://api.brewerydb.com/v2/";
  private static String apiKey = "?key=1511d0db4a1d6841481c672455358cff";

  private static Gson gson = new Gson();
  private static JsonParser parser = new JsonParser();

  public static List<Beer> getBeerListForStyle(int styleId) throws IOException {
    String url = baseUrl + "/beers/" + apiKey + "&styleId=" + styleId;

    JsonElement responseWrapper = getJson(url);

    Type beerListType = new TypeToken<ArrayList<Beer>>() {
    }.getType();
    return gson.fromJson(responseWrapper, beerListType);
  }

  public static List<Style> loadBeerStyles() throws IOException {
    String url = baseUrl + "/styles/" + apiKey;

    JsonElement responseWrapper = getJson(url);

    Type styleListType = new TypeToken<ArrayList<Style>>() {
    }.getType();
    return gson.fromJson(responseWrapper, styleListType);
  }

  public static List<Beer> getBeerById(String id) throws IOException {
    String url = baseUrl + "/beers/" + apiKey + "&ids=" + id;
    JsonElement responseWrapper = getJson(url);
    Type beerListType = new TypeToken<ArrayList<Beer>>() {
    }.getType();
    return gson.fromJson(responseWrapper, beerListType);
  }

  /**
   * gets the JsonElement containing the results of the request with the url parameter
   *
   * @param url
   * @return JsonElement containing the results
   * @throws IOException
   */
  private static JsonElement getJson(String url) throws IOException {
    try (InputStream is = new URL(url).openStream()) {
      BufferedReader reader = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
      JsonObject element = (JsonObject) parser.parse(reader);

      return element.get("data");
    }
  }
}


