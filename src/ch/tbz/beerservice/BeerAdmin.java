package ch.tbz.beerservice;

import ch.tbz.beerservice.api.ApiConnection;
import ch.tbz.beerservice.beers.Beer;
import ch.tbz.beerservice.styles.Style;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class BeerAdmin {

	/**
	 * prints all beer styles of the beerservice
	 */
	public void printBeerStyles() {
		try {
			List<Style> styleList = ApiConnection.loadBeerStyles();
			for (Style style: styleList) {
				System.out.println(style.toString());
			}
			System.out.println(styleList.size() + "results found");

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * prints all beer styles that contain the search term
	 * @param search the search term
	 */
	public void printBeerStyles(String search) {
		try {
			List<Style> matches = new ArrayList<>();
			List<Style> styleList = ApiConnection.loadBeerStyles();
			for (Style style: styleList) {
				if(style.getName().toLowerCase().contains(search.toLowerCase())) {
					matches.add(style);
				}
			}
			for (Style style: matches) {
				System.out.println(style.toString());
			}
			System.out.println(matches.size() + " results found");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * prints all beers of the style with the styleId
	 * @param styleId the id of the style
	 */
	public void printBeerList(int styleId) {
		try {
			List<Beer> beerList = ApiConnection.getBeerListForStyle(styleId);
			for (Beer beer: beerList) {
				System.out.println(beer.toString());
			}
			System.out.println(beerList.size() + "results found");

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * prints out the beer and its description with the id
	 * @param id the id of the beer
	 */
	public void printBeer(String id) {
		try {
			if (ApiConnection.getBeerById(id) == null) {
				// if there is no beer found with the id
				System.out.println("No beer found with the id [" + id + "]");
				return;
			}
			Beer beer = ApiConnection.getBeerById(id).get(0);
			if(beer.getDescription() == null){
				// if there is no description provided
				beer.setDescription("no description provided");
			}
			System.out.println(beer.getBeerInformation());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}




