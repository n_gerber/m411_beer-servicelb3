package ch.tbz.beerservice;

import com.sun.tools.doclets.formats.html.SourceToHTMLConverter;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Beerservice {

  public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		BeerAdmin beeradmin = new BeerAdmin();

		// User interface
		while(true) {
			try {
				System.out.println("___________________________________________");
				System.out.println(" ");
				System.out.println("1. print all beer styles");
				System.out.println("2. search for beer styles");
				System.out.println("3. print all beers with the same style id");
				System.out.println("4. print beer with id");
				System.out.println("5. exit program");
				System.out.println("___________________________________________\n\n");
				System.out.println("Enter a number: ");
				int choice = sc.nextInt();

				switch (choice) {
					case 1:
						beeradmin.printBeerStyles();
						break;
					case 2:
						System.out.print("was geehr ");
						String search = sc.next();
						beeradmin.printBeerStyles(search);
						break;
					case 3:
						System.out.print("enter style ID: ");
						int styleId = sc.nextInt();
						beeradmin.printBeerList(styleId);
						break;
					case 4:
						System.out.print("enter beer ID: ");
						String beerId = sc.next();
						beeradmin.printBeer(beerId);
						break;
					case 5:
						System.out.println("Good Bye!");
						System.exit(0);
						break;
					default:
						System.out.println("please enter a valid number");
				}
			} catch (InputMismatchException e) {
					System.out.println("please enter a number");
					sc.nextLine();
			}
		}
  }
}
