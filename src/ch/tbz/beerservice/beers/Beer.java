package ch.tbz.beerservice.beers;

public class Beer {

  private String id;
  private String name;
  private String description;
  private int styleId;

  public Beer(String id,
              String name,
              String description,
              int styleId) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.styleId = styleId;
  }

  public String getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public String getDescription() {
    return description;
  }

  public int getStyleId() {
    return styleId;
  }

  @Override
  public String toString() {
    return this.id + "::" + this.name;
  }

  public String getBeerInformation() {
  	return this.toString() + "\n" + this.description;
  }

	public void setDescription(String description) {
		this.description = description;
	}
}
